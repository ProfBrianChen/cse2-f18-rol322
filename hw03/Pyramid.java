/// CSE hw03 Ronan Leahy
/// Part 2
/// Write a program that prompts the user for the dimensions of a pyramid and returns the volume inside the pyramid

import java.util.Scanner;
  
public class Pyramid {
  public static void main (String [] args) {
    Scanner myScanner = new Scanner (System.in); // scanner declaration
    
    System.out.print ("Please input the length of the pyramid base: ");
      double baseLength = myScanner.nextDouble(); // user input for base length
    System.out.print ("Please input the height of the pyramid: ");
      double height = myScanner.nextDouble(); // user input for pyramid height
    
      double volume = (((baseLength * baseLength) * height) / 3); // calculates volume using base length and pyramid height
    System.out.println ("The volume of the pyramid is: " + volume + " cubic feet"); // prints volume
  }
}