/// CSE hw03 Ronan Leahy
/// Part 1
/// Write a program that asks the user for doubles that represent 
///the number of acres of land affected by hurricane precipitation 
///and how many inches of rain were dropped on average.

import java.util.Scanner;

public class Convert {
  public static void main (String [] args) {
    Scanner myScanner = new Scanner (System.in); //Scanner declaration
    
    System.out.println ("There is a hurricane in your area!");
    System.out.println ("");
    System.out.print ("How many acres of land are affected by hurricane precipitation: ");
      double numAcres = myScanner.nextDouble(); // number of acres affected by hurricane rain
    System.out.print ("How many inches of rain were dropped on average: ");
      double inchesOfRain = myScanner.nextDouble(); // number of inches of rain dropped
    
    double rainInGallons = numAcres * (inchesOfRain * 27154); // the amount of rain in gallons
    double rainInCubicMiles = rainInGallons * 9.0817e-13; // the amount of rain in cubic miles
    
    System.out.println ("There was a total of " + rainInCubicMiles + " cubic miles of rain");
    
  }
}