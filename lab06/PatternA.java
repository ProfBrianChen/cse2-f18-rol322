// CSE2 Ronan Leahy Lab06 part 1

import java.util.*;

public class PatternA {
  public static void main (String [] args) {
    Scanner myScanner = new Scanner (System.in);
    int userInput = 0;
    boolean intEntered = false;
    String temp = "";
    int i = 1;
    
    
    System.out.println ("Enter a number from 1-10");
    while (intEntered == false) { // tests to see if user has assigned a value yet
      if (myScanner.hasNextInt()) { // tests for an int value
        userInput = myScanner.nextInt(); // sets a value equal to user input
        if (userInput <= 10 && userInput > 0) {
          intEntered = true; // changes conditional to end loop
        }
        else {
        System.out.println ("Error:"); // error message
        System.out.println ("Enter a number from 1-10");
        }
      }
      else {
        temp = myScanner.next(); // assigns user input to a temp value if they input wrong type
        System.out.println ("Error:"); // error message
        System.out.println ("Enter a number from 1-10");
      }
      
    }

    
    for (int numRows = 0; numRows <= userInput; numRows++) {
      while (i <= numRows) {
        System.out.print (i + " ");
        i++;
      }
      System.out.println("");
      i = 1;
    }
  }
}