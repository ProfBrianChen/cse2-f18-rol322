/// CSE Lab03 Ronan Leahy
/// Using Scanner to find out the proper payment for a dinner bill

import java.util.Scanner;

public class Check {
  public static void main (String [] args) {
    Scanner myScanner = new Scanner (System.in); // Scanner declaration
    
    System.out.print ("Please enter the original cost of the check in the form xx.xx: ");
      double checkCost = myScanner.nextDouble(); // the amount the user wants the check to cost
    System.out.print ("Please enter the perecentage tip that you wish to pay as a whole number in the form xx: ");
      double tipPercent = myScanner.nextDouble();
      tipPercent = tipPercent / 100; // the amount of tip being added onto the bill
    System.out.print ("Please enter the number of people that went out to dinner: ");
      int numPeople = myScanner.nextInt(); // number of people that will split the bill
    
    double totalCost; // total cost of bill
    double costPerPerson; // amount each person will pay
    int dollars , dimes , pennies; // for storing digits to the left and right of the decimal
    
    totalCost = checkCost * (1 + tipPercent);
    costPerPerson = totalCost / numPeople;
    dollars = (int) costPerPerson; // cost in dollars, drops the decimal
    dimes = (int) (costPerPerson * 10) % 10; //returns the remainder
    pennies = (int) (costPerPerson * 100) % 10;
    
    System.out.println ("Each person in the group needs to pay $" + dollars + "." + dimes + pennies);
    
  }
}