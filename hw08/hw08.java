/// CSE2 Ronan Leahy Shuffle


import java.util.*;

  public class hw08{ 
    public static void main(String[] args) { 
      Scanner scan = new Scanner(System.in); 
 //suits club, heart, spade or diamond 
      String[] suitNames={"C","H","S","D"};    
      String[] rankNames={"2", "3", "4", "5", "6", "7", "8", "9", "10", "J", "Q","K","A"}; 
      String[] cards = new String[52]; 
      String[] hand = new String[5]; 
      int numCards = 0; 
      int again = 1; 
      int index = 51;
      
  for (int i=0; i<52; i++){ 
    cards[i]=rankNames[i%13]+suitNames[i/13]; 
    System.out.print(cards[i]+" "); 
    } 
      
      System.out.println();
      cards = shuffle(cards); 
      printArray(cards); 
      System.out.println ("Shuffled");
      
  while(again == 1){ 
   System.out.println ("How many cards would you like to draw. Pick a number between 1 and 5");
   numCards = scan.nextInt();
   if (index - numCards < 0) {
     System.out.println ("Ran out of cards: new deck created");
     printArray(cards);
     cards = shuffle(cards);
     index = 51;
   }
   hand = getHand(cards,index,numCards); 
   printArray(hand);
   index = index - numCards;
   System.out.println("Enter a 1 if you want another hand drawn"); 
   again = scan.nextInt(); 
}  
  }
    
    public static String[] shuffle (String [] list) {
      for (int i = 0; i < 100; i++) {
        Random rand = new Random ();
        int x = rand.nextInt(51) + 1;
        String temp = list[0];   
        list[0] = list[x];
        list[x] = temp;
      }
      return list;
    }
    
    public static String[] getHand (String [] list, int index, int numCards) {
      String[] hand = new String[numCards]; 
      System.out.println ("Hand");
      for (int i = 0; i < numCards; i++) {
        hand[i] = list[index];
        index = index - 1;
      }
      return hand;
    }
    
    public static void printArray (String [] list) {
      System.out.println();
      for (int i = 0; i < list.length; i++) {
        System.out.print (list[i] + " ");
      }
    }
}
