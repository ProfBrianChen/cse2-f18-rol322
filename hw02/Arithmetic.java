/// CSE2 hw02 Ronan Leahy

public class Arithmetic {
 public static void main (String [] args) {
  
   double paSalesTax = 0.06; //Tax rate for Pennsylvania
  
   int numPants = 3; // Number of pants
   double pantsPrice = 34.98; // Cost of a pair of pants
   double totalCostOfPants = pantsPrice * (1 + paSalesTax); // total cost of pants after tax
   totalCostOfPants = (int) (totalCostOfPants * 100) / 100.0; // total cost of pants with tax to 2 decimals
   double pantsSalesTax = pantsPrice * paSalesTax; // amount of tax paid on a pair of pants
   pantsSalesTax = (int) (pantsSalesTax * 100) / 100.0; // tax paid in 2 decimals
  
   int numShirts = 2; // Number of sweatshirts
   double shirtPrice = 24.99; // Cost of a sweatshirt
   double totalCostOfShirt = shirtPrice * (1 + paSalesTax); // total cost of a sweatshirt after tax
   totalCostOfShirt = (int) (totalCostOfShirt * 100) / 100.0; // total cost of a sweatshirt after tax 2 decimals
   double shirtSalesTax = shirtPrice * paSalesTax; // amount of tax paid on one sweatshirt
   shirtSalesTax = (int) (shirtSalesTax * 100) / 100.0; // tax paid in 2 decimals
  
   int numBelts = 1; // Number of belts
   double beltCost = 33.99; // Cost of a belt
   double totalCostOfBelt = beltCost * (1 + paSalesTax); // total cost of one belt after tax
   totalCostOfBelt = (int) (totalCostOfBelt * 100) / 100.0; // total cost of a belt after tax 2 decimals
   double beltSalesTax = beltCost * paSalesTax; // amount of tax paid for one belt
   beltSalesTax = (int) (beltSalesTax * 100) / 100.0; // tax paid in 2 decimals
  
   double totalSalesTax = (3 * pantsSalesTax) + (2 * shirtSalesTax) + beltSalesTax; // total amount of tax paid
   totalSalesTax = (int) (totalSalesTax * 100) / 100.0; // total sales tax 2 decimal points
   double totalAmountPaid = (3 * totalCostOfPants) + (2 * totalCostOfShirt) + totalCostOfBelt; // total amount paid with tax
   totalAmountPaid = (int) (totalAmountPaid * 100) / 100.0; // total cost to 2 decimal places
   double noTaxPayment = (3 * pantsPrice) + (2 * shirtPrice) + beltCost; // total amount paid not inlcuding tax
  
   System.out.println ("The cost of a pair of pants is $" + pantsPrice + " and with tax $" + totalCostOfPants);
   System.out.println ("The amount of tax per pair of pants is $" + pantsSalesTax);
   System.out.println ("The cost of a sweatshirt is $" + shirtPrice + " and with tax $" + totalCostOfShirt);
   System.out.println ("The amount of tax per sweatshirt is $" + shirtSalesTax);
   System.out.println ("The cost of a belt is $" + beltCost + " and with tax $" + totalCostOfBelt);
   System.out.println ("The amount of tax per belt is $" + beltSalesTax);
   System.out.println ("");
   System.out.println ("The cost of purchasing 3 pants, 2 sweatshirts, and 1 belt before tax is $" + noTaxPayment);
   System.out.println ("");
   System.out.println ("The total amount of tax paid on this purchase is $" + totalSalesTax);
   System.out.println ("");
   System.out.println ("The total cost with sales tax is $" + totalAmountPaid);
   System.out.println ("");
  
 }
}
