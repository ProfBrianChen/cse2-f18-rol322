/// CSE02 Lab04
/// Ronan Leahy
/// Generates a random card from a stack of 52

import java.util.*;

public class CardGenerator {
  public static void main (String [] args) {
    int cardNum = (int) (Math.random()*52) +1;
    String suitName = ""; // name of suit
    String cardName = ""; // name of card
    
    if ( cardNum <= 13 ) { // if number is between 1-13 it's diamond
      suitName = "Diamonds";
      cardName = "" + cardNum; // sets card name to card number
      if ( cardNum == 1) { // ace tester
        cardName = "Ace";
      }
      if ( cardNum == 11 ) { // jack tester
        cardName = "Jack";
      }
      if ( cardNum == 12 ) { // queen tester
        cardName = "Queen";
      }
      if ( cardNum == 13 ) {
        cardName = "King"; // king tester
      }
    }
    if ( 14 <= cardNum && cardNum <= 26 ) { // same code for club suit
      suitName = "Clubs";
      cardName = "" + (cardNum - 13);
      if ( cardNum == 14) {
        cardName = "Ace";
      }
      if ( cardNum == 24 ) {
        cardName = "Jack";
      }
      if ( cardNum == 25 ) {
        cardName = "Queen";
      }
      if ( cardNum == 26 ) {
        cardName = "King";
      }
    }
    if ( 27 <= cardNum && cardNum <= 39 ) { // same code for hearts suit
      suitName = "Hearts";
      cardName = "" + (cardNum - 26);
      if ( cardNum == 27) {
        cardName = "Ace";
      }
      if ( cardNum == 37 ) {
        cardName = "Jack";
      }
      if ( cardNum == 38 ) {
        cardName = "Queen";
      }
      if ( cardNum == 39 ) {
        cardName = "King";
      }
    }
    if ( 40 <= cardNum && cardNum <= 52 ) { // same code for spade suit
      suitName = "Spades";
      cardName = "" + (cardNum - 39);
        if ( cardNum == 40) {
        cardName = "Ace";
      }
      if ( cardNum == 50 ) {
        cardName = "Jack";
      }
      if ( cardNum == 51 ) {
        cardName = "Queen";
      }
      if ( cardNum == 52 ) {
        cardName = "King";
      }
    }
    
    System.out.println ("You picked the " + cardName + " of " + suitName); // prints card info
    
  }
}