/**
 *
 * @author RonanLeahy CSE2 hw07
 * A word generator that can manipulate a string of texts
 * based on user input
 */

import java.util.*;

public class WordTools {
    
    static int counterWhiteSpace = 0; // used for counting white space
    static int counterWords = 1; // used for counting number of words
    static int counterPhrase = 0; // used for searching for phrases
    
    public static void main(String[] args) {
        
        // variable declaration
        boolean running = true; 
        String sample = "";
        String menu = "";
        String phrase = "";
        Scanner myScanner = new Scanner (System.in);
        
        System.out.println ("Enter a sample text");
        
        sample = myScanner.nextLine();
        sampleText(sample);
        
        // all choices for the user they each run an individual method
        System.out.println ("MENU \n");
        System.out.println ("c - Number of non-whitesapce characters");
        System.out.println ("w - Number of words");
        System.out.println ("f - Find text");
        System.out.println ("r - replace all !'s");
        System.out.println ("s - Shorten spaces");
        System.out.println ("q - Quit \n");
        System.out.println ("Choose an Option");
        
        // loop for entering menu options
        while (running) {
            menu = myScanner.nextLine();
            // tests to see if the choice was valid
            if (menu.equalsIgnoreCase("c") || menu.equalsIgnoreCase("w") || menu.equalsIgnoreCase("f") || menu.equalsIgnoreCase("r") || menu.equalsIgnoreCase("s") || menu.equalsIgnoreCase("q") ) {
                // tests to see if they chose to scan for a phrase 
                // and if so declares the variable as the phrase
                if (menu.equalsIgnoreCase("f")) {
                    System.out.println ("What phrase or word would you like to look for");
                    phrase = myScanner.nextLine();
                }
                // passes all variables to the printMenu method
                printMenu (menu , sample , phrase);
                
            }
            else {
                System.out.println ("Enter a valid choice.");
            }
        }
        
        System.exit(0);
    }
    
    public static void sampleText (String x) {
        
        System.out.println ("You entered: " + x);
        
    }
    
    public static void printMenu (String y , String text , String phrase) {
        // Enters multiple methods based on user input
        // y is user input, text is the original text, and phrase is 
        // the text they entered if they input 'f'
        switch (y) {
            case "c":
                getNumOfNonWSCharacters(text);
                break;
            case "C":
                getNumOfNonWSCharacters(text);
                break;
            case "w":
                getNumOfWords(text);
                break;
            case "W":
                getNumOfWords(text);
                break;
            case "f":
                findText(text , phrase);
                break;
            case "F":
                findText(text , phrase);
                break;
            case "r":
                System.out.println (replaceExclamation(text));
                break;
            case "R":
                System.out.println (replaceExclamation(text));
                break;
            case "s":
                System.out.println (shortenSpace(text));
                break;
            case "S":
                System.out.println (shortenSpace(text));
                break;
            case "q":
                quit();
                break;
            case "Q":
                quit();
                break;
             
    }
        
        System.out.println ("Choose another Option");
        System.out.println ("Press 'q' to quit");
    }
    
    public static void getNumOfNonWSCharacters (String text) {
        // runs through each char in the text and counts nonwhitespace characters
        for (int i = 0; i < text.length(); i++) {
            if (text.charAt(i) == ' ') {
                
            }
            else {
                counterWhiteSpace++;
            }
        }
        System.out.println ("Number of non-whitespace characters: " + counterWhiteSpace);
    }
    
    public static void getNumOfWords (String text) {
        // goes through each char and ticks the number of words
        // up when it reaches a space. The counter starts at 1 
        // to account for the last word in the string
        for (int i = 0; i < text.length(); i++) {
            if (text.charAt(i) == ' ' && text.charAt(i + 1) != ' ') {
                counterWords++;
            }
            else {
                
            }
        }
        System.out.println ("Number of words: " + counterWords);
    }
    
    public static void findText (String text , String phrase) {
        // removes the phrase from the text and finds the difference
        // from the old length to the new length and then divides
        // by the phrase length
        int phraseLength = phrase.length();
        int initialLength = text.length();
        text = text.replaceAll (phrase, "");
        int newLength = text.length();
        counterPhrase = (initialLength - newLength) / phraseLength;
        System.out.println ("'" + phrase + "' instances: " + counterPhrase);
    }
    
    public static String replaceExclamation (String text) {
        // string method to replace all '!' with '.'
        text = text.replaceAll ("!" , ".");
        return text;
    }
    
    public static String shortenSpace (String text) {
        // replaces double spaces with single spaces
        text = text.replaceAll ("( )+ " , " ");
        return text;
    }
    
    public static void quit () {
        // exits the program
        System.exit(0);
    }
    
    
}