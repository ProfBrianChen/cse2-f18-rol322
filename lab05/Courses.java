// CSE2 Ronan Leahy Lab05
/* Your program is to write loops that asks the user to enter information 
relating to a course they are currently taking - ask for the course number, 
department name, the number of times it meets in a week, the time the class starts, 
the instructor name, and the number of students.  For each item, check to make 
sure that what the user enters is actually of the correct type, and if not, use 
an infinite loop to ask again. So if an integer needs to be entered, make sure 
an integer is entered, and loop until one actually is. Use next() to remove 
unwanted words from the conveyor belt when users do not type in a word of the correct type.  
When a user types in an unwanted word (e.g. the wrong type) print an error message saying 
that you need input of integer type or string type, etc. */

import java.util.Scanner;

public class Courses {
  
  public static void main (String[] args) {
    
    Scanner myScanner = new Scanner (System.in); //Scanner declaration
    // conditionals used for while loops
    boolean courseNumber = false;
    boolean departmentName = false;
    boolean weeklyMeetings = false;
    boolean classTime = false;
    boolean instructorName = false;
    boolean numOfSTudents = false;
    // values of user inputs
    int course = 0;
    String department = "";
    int weekly = 0;
    int time = 0;
    String instructor = "";
    int students = 0;
    String temp = "";
    
    System.out.println ("Enter the course number");
    while (courseNumber == false) { // tests to see if user has assigned a value yet
      if (myScanner.hasNextInt()) { // tests for an int value
        course = myScanner.nextInt(); // sets course equal to user input
        courseNumber = true; // changes conditional to end loop
      }
      else {
        temp = myScanner.next(); // assigns user input to a temp value if they input wrong type
        System.out.println ("Error: Input an integer value"); // error message
        System.out.println ("Enter the course number");
      }
      
    }
    
    System.out.println ("Enter the department name");
    while (departmentName == false) {
      if (myScanner.hasNext() && !myScanner.hasNextInt() && !myScanner.hasNextDouble()) {
        department = myScanner.next();
        departmentName = true;
      }
      else {
        temp = myScanner.next();
        System.out.println ("Error: Input a String value");
        System.out.println ("Enter the department name");
      }
      
    }
    
    System.out.println ("Enter the number of time it meets in a week");
    while (weeklyMeetings == false) {
      if (myScanner.hasNextInt()) {
        weekly = myScanner.nextInt();
        weeklyMeetings = true;
      }
      else {
        temp = myScanner.next();
        System.out.println ("Error: Input an integer value");
        System.out.println ("Enter the number of time it meets in a week");
      }
      
    }
    
    
    System.out.println ("Enter the time the class starts");
    while (classTime == false) {
      if (myScanner.hasNextInt()) {
        time = myScanner.nextInt();
        classTime = true;
      }
      else {
        temp = myScanner.next();
        System.out.println ("Error: Input an integer value");
        System.out.println ("Enter the time the class starts");
      }
      
    }
    
    
    System.out.println ("Enter the instructor name");
    while (instructorName == false) {
      if (myScanner.hasNext() && !myScanner.hasNextInt() && !myScanner.hasNextDouble()) {
        instructor = myScanner.next();
        instructorName = true;
      }
      else {
        temp = myScanner.next();
        System.out.println ("Error: Input a String value");
        System.out.println ("Enter the instructor name");
      }
      
    }
    
    
    System.out.println ("Enter the number of students");
    while (numOfSTudents == false) {
      if (myScanner.hasNextInt()) {
        students = myScanner.nextInt();
        numOfSTudents = true;
      }
      else {
        temp = myScanner.next();
        System.out.println ("Error: Input an integer value");
        System.out.println ("Enter the number of students");
      }
      
    }
    
    // Print out user inputs
    System.out.println ("Course Number: " + course);
    System.out.println ("Department: " + department);
    System.out.println ("Number of weekly meetings: " + weekly);
    System.out.println ("Time of class: " + time);
    System.out.println ("Instructor: " + instructor);
    System.out.println ("Number of students: " + students);
    
    
    
  }
}