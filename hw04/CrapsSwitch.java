/// Ronan Leahy CSE02 HW04 Part 2
// evaluate common craps names using switch statements

import java.util.*;

public class CrapsSwitch {
  public static void main (String [] args) {
    Scanner myScanner = new Scanner (System.in);
    Random myRandom = new Random();
    String crapRoll = "";
    int dice1 = 0;
    int dice2 = 0;
    
    System.out.println ("What would you like to do?"); // asks for user inputs
    System.out.println ("1: Randomly cast dice");
    System.out.println ("2: State dice values to evaluate");
    int choice1 = myScanner.nextInt(); // stores value for user input
    
    switch ( choice1 ) {
      case 1:
        dice1 = myRandom.nextInt(6) + 1; // creates random numbers for both die
        dice2 = myRandom.nextInt(6) + 1;
        break;
      case 2:
        System.out.println ("Please choose a value for the first dice"); // asks user to choose dice values
        dice1 = myScanner.nextInt();
          switch ( dice1 ) {
            case 1:
              dice1 = 1;
              break;
            case 2:
              dice1 = 2;
              break;
            case 3:
              dice1 = 3;
              break;
            case 4:
              dice1 = 4;
              break;
            case 5:
              dice1 = 5;
              break;
            case 6:
              dice1 = 6;
              break;
            default:
              System.out.println ("You did not choose a valid number. The value has been set to 6"); // sets die value if user inputs an invalid number
              dice1 = 6;
              break;
          }
            
         System.out.println ("Please choose a value for the second dice");
         dice2 = myScanner.nextInt();
          switch ( dice2 ) {
            case 1:
              dice2 = 1;
              break;
            case 2:
              dice2 = 2;
              break;
            case 3:
              dice2 = 3;
              break;
            case 4:
              dice2 = 4;
              break;
            case 5:
              dice2 = 5;
              break;
            case 6:
              dice2 = 6;
              break;
            default:
              System.out.println ("You did not choose a valid number. The value has been set to 6"); // sets die value if user inputs an invalid number
              dice2 = 6;
              break;
          }
            break;
    }
        switch ( dice1 ) {
          case 1:
          switch ( dice2 ) {
            case 1:
              crapRoll = "Snake Eyes";
              break;
            case 2:
              crapRoll = "Ace Deuce";
              break;
            case 3:
              crapRoll = "Easy Four";
              break;
            case 4:
              crapRoll = "Fever Five";
              break;
            case 5:
              crapRoll = "Easy Six";
              break;
            case 6:
              crapRoll = "Seven Out";
              break;
            default:
              break;
            } break;
          case 2:
          switch ( dice2 ) {
            case 1:
               crapRoll = "Ace Deuce";
            case 2:
              crapRoll = "Hard Four";
              break;
            case 3:
              crapRoll = "Fever Five";
              break;
            case 4:
              crapRoll = "Easy Six";
              break;
            case 5:
              crapRoll = "Seven Out";
              break;
            case 6:
              crapRoll = "Easy Eight";
              break;
            default:
              break;
            } break;
          case 3:
          switch ( dice2 ) {
            case 1:
              crapRoll = "Easy Four";
              break;
            case 2:
              crapRoll = "Fever Five";
              break;
            case 3:
              crapRoll = "Hard Six";
              break;
            case 4:
              crapRoll = "Seven Out";
              break;
            case 5:
              crapRoll = "Easy Eight";
              break;
            case 6:
              crapRoll = "Nine";
              break;
            default:
              break;
            } break;
          case 4:
          switch ( dice2 ) {
            case 1:
              crapRoll = "Fever Five";
              break;
            case 2:
              crapRoll = "Easy Six";
              break;
            case 3:
              crapRoll = "Seven Out";
              break;
            case 4:
              crapRoll = "Hard Eight";
              break;
            case 5:
              crapRoll = "Nine";
              break;
            case 6:
              crapRoll = "Easy Ten";
              break;
            default:
              break;
            } break;
          case 5:
          switch ( dice2 ) {
            case 1:
              crapRoll = "Easy Six";
              break;
            case 2:
              crapRoll = "Seven Out";
              break;
            case 3:
              crapRoll = "Easy Eight";
              break;
            case 4:
              crapRoll = "Nine";
              break;
            case 5:
              crapRoll = "Hard Ten";
              break;
            case 6:
              crapRoll = "Yo-leven";
              break;
            default:
              break;
            } break;
          case 6:
          switch ( dice2 ) {
            case 1:
              crapRoll = "Seven Out";
              break;
            case 2:
              crapRoll = "Easy Eight";
              break;
            case 3:
              crapRoll = "Nine";
              break;
            case 4:
              crapRoll = "Easy Ten";
              break;
            case 5:
              crapRoll = "Yo-leven";
              break;
            case 6:
              crapRoll = "Boxcars";
              break;
            default:
              break;
            } break;
          }
            System.out.println ("You rolled a " + crapRoll + ", thank you for playing!"); // prints out slang terminology
            
            
          }
        }