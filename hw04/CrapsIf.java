/// Ronan Leahy CSE02 HW04 Part 1
// Evaluate common craps names using if statements

import java.util.*;

public class CrapsIf {
  public static void main (String [] args) {
    Scanner myScanner = new Scanner (System.in);
    Random myRandom = new Random();
    String crapRoll = ""; // variable declaration
    int dice1 = 0;
    int dice2 = 0;
    
    System.out.println ("What would you like to do?"); // asks for user inputs
    System.out.println ("1: Randomly cast dice");
    System.out.println ("2: State dice values to evaluate");
    int choice1 = myScanner.nextInt(); // stores value for user input
    
    if ( choice1 == 1 ) {
     dice1 = myRandom.nextInt(6) + 1; // creates random numbers for both die
     dice2 = myRandom.nextInt(6) + 1;
    }
    else if ( choice1 == 2 ) {
      System.out.println ("Please choose a value for the first dice"); // asks user to choose dice values
        dice1 = myScanner.nextInt();
          if ( dice1 > 6 || 0 > dice1) {
            dice1 = 6;
            System.out.println ("You did not choose a valid number. The value has been set to 6"); // sets die value if user inputs an invalid number
          }
      System.out.println ("Please choose a value for the second dice");
        dice2 = myScanner.nextInt();
          if ( dice2 > 6 || 0 > dice2) {
            dice2 = 6;
            System.out.println ("You did not choose a valid number. The value has been set to 6");
          }
    }
    else if ( choice1 != 1 || choice1 != 2 ) {
      System.out.println ("Please restart and choose a valid option"); // exits the code if the user chooses an invalid option
      System.exit(0);
    } 
    
    if ( dice1 == 1 && dice2 == 1 ) { // tests dice values to properly name the roll
        crapRoll = "Snake Eyes";
      }
    if (( dice1 == 1 && dice2 == 2 ) || ( dice1 == 2 && dice2 == 1 )) { // tests dice values to properly name the roll
        crapRoll = "Ace Deuce";
      }
    if (( dice1 == 1 && dice2 == 3 ) || ( dice1 == 3 && dice2 == 1 )) { // tests dice values to properly name the roll
        crapRoll = "Easy Four";
      }
    if (( dice1 == 1 && dice2 == 4 ) || ( dice1 == 4 && dice2 == 1 )) { // tests dice values to properly name the roll
        crapRoll = "Fever Five";
      }
    if (( dice1 == 1 && dice2 == 5 ) || ( dice1 == 5 && dice2 == 1 )) { // tests dice values to properly name the roll
        crapRoll = "Easy Six";
      }
    if (( dice1 == 1 && dice2 == 6 ) || ( dice1 == 6 && dice2 == 1 )) { // tests dice values to properly name the roll
        crapRoll = "Seven Out";
      }
    if ( dice1 == 2 && dice2 == 2 ) { // tests dice values to properly name the roll
        crapRoll = "Hard Four";
      }
    if (( dice1 == 2 && dice2 == 3 ) || ( dice1 == 3 && dice2 == 2 )) { // tests dice values to properly name the roll
        crapRoll = "Fever Five";
      }
    if (( dice1 == 2 && dice2 == 4 ) || ( dice1 == 4 && dice2 == 2 )) { // tests dice values to properly name the roll
        crapRoll = "Easy Six";
      }
    if (( dice1 == 2 && dice2 == 5 ) || ( dice1 == 5 && dice2 == 2 )) { // tests dice values to properly name the roll
        crapRoll = "Seven Out";
      }
    if (( dice1 == 2 && dice2 == 6 ) || ( dice1 == 6 && dice2 == 2 )) { // tests dice values to properly name the roll
        crapRoll = "Easy Eight";
      }
    if ( dice1 == 3 && dice2 == 3 ) { // tests dice values to properly name the roll
        crapRoll = "Hard Six";
      }
    if (( dice1 == 3 && dice2 == 4 ) || ( dice1 == 4 && dice2 == 3 )) { // tests dice values to properly name the roll
        crapRoll = "Seven Out";
      }
    if (( dice1 == 3 && dice2 == 5 ) || ( dice1 == 5 && dice2 == 3 )) { // tests dice values to properly name the roll
        crapRoll = "Easy Eight";
      }
    if (( dice1 == 3 && dice2 == 6 ) || ( dice1 == 6 && dice2 == 3 )) { // tests dice values to properly name the roll
        crapRoll = "Nine";
      }
    if ( dice1 == 4 && dice2 == 4 ) { // tests dice values to properly name the roll
        crapRoll = "Hard Eight";
      }
    if (( dice1 == 4 && dice2 == 5 ) || ( dice1 == 5 && dice2 == 4 )) { // tests dice values to properly name the roll
        crapRoll = "Nine";
      }
    if (( dice1 == 4 && dice2 == 6 ) || ( dice1 == 6 && dice2 == 4 )) { // tests dice values to properly name the roll
        crapRoll = "Easy Ten";
      }
    if ( dice1 == 5 && dice2 == 5 ) { // tests dice values to properly name the roll
        crapRoll = "Hard Ten";
      }
    if (( dice1 == 5 && dice2 == 6 ) || ( dice1 == 6 && dice2 == 5 )) { // tests dice values to properly name the roll
        crapRoll = "Yo-leven";
      }
    if ( dice1 == 6 && dice2 == 6 ) { // tests dice values to properly name the roll
        crapRoll = "Boxcars";
      }
    
    System.out.println ("You rolled a " + crapRoll + ", thank you for playing!"); // prints out slang terminology
    
  }
}