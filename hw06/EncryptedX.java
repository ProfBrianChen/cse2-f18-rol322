// CSE2 Ronan Leahy hw06

import java.util.*;

public class EncryptedX {
  public static void main (String [] args) {
    Scanner myScanner = new Scanner (System.in);
    int userInput = 0;
    boolean intEntered = false;
    String temp = "";
    int i = 0;
    boolean printing = true;
    
    
    System.out.println ("Enter a number from 0-100");
    while (intEntered == false) { // tests to see if user has assigned a value yet
      if (myScanner.hasNextInt()) { // tests for an int value
        userInput = myScanner.nextInt(); // sets a value equal to user input
        if (userInput <= 100 && userInput > 0) {
          intEntered = true; // changes conditional to end loop
        }
        else {
        System.out.println ("Error:"); // error message
        System.out.println ("Enter a number from 0-100");
        }
      }
      else {
        temp = myScanner.next(); // assigns user input to a temp value if they input wrong type
        System.out.println ("Error:"); // error message
        System.out.println ("Enter a number from 0-100");
      }
      
    }
    
    for (int numRows = 0; numRows <= userInput; numRows++) {
        for (int numCols = 0; numCols <= userInput; numCols++) {
          if (numCols == numRows || (numCols + i) == userInput) {
            System.out.print (" ");
          }
          else {
            System.out.print ("x");
          }
        }
      System.out.println ("");
      i++;
     }
    
    }
    
  }