// CSE2 hw05 Ronan Leahy
/* Your program should ask the user how many times it should generate hands. 
For each hand, your program should generate five random numbers from 1 to 52, 
where these 52 cards represent the four sets of 13 cards associated with each suit.  
Specifically, cards 1-13 represent diamonds; 14-26 represent clubs; then hearts; 
then spades.  In all suits, card identities ascend in step with the card number.  
For example, 14 is the ace of clubs; 15 is the 2 of clubs; and 26 is the king of clubs.  
Then, your program should check if this set of five cards belongs to one of the four 
hands listed in the above. If so, the counter for this hand should be incremented by one. 
After all hands have been generated, your program should calculate the probabilities of 
each of the four hands described above by computing the number of occurrences divided by 
the total number of hands. Note: You must also guard against duplicate cards generated in 
one hand.  For example, if the first card drawn is 14 (= ace of clubs), you must ensure that 
this card is not drawn again for the four remaining cards in that hand.
*/

import java.util.*;

public class Hw05 {
  public static void main (String [] args) {
    Random myRandom = new Random(); // random number generator
    Scanner myScanner = new Scanner (System.in); // scanner declaration
    
    int numberOfHands = 0;
    boolean userInput = false;
    
    int randNum1 = 0;
    int num1Value = 0;
    int randNum2 = 0;
    int num2Value = 0;
    int randNum3 = 0;
    int num3Value = 0;
    int randNum4 = 0;
    int num4Value = 0;
    int randNum5 = 0;
    int num5Value = 0;
    
    boolean cardChecker = false;
    
    int pairChecker = 0;
    int pair2Checker = 0;
    boolean pairs = false; // boolean to see if any pair is existent
   
   
    // increments everytime the type of hand is produced
    int onePair = 0;
    int twoPair = 0;
    int threeOfAKind = 0;
    int fourOfAKind = 0;
    // seperates the first and second pairs
    int pairOne = 0;
    int pairTwo = 0;
    
    int counter = 0;
    
    System.out.println ("How many hands would you like to generate");
    while (userInput == false) {
    if (myScanner.hasNextInt()) {
    numberOfHands = myScanner.nextInt();
    userInput = true;
    }
    else {
    myScanner.next();
    System.out.println ("ERROR: Input an integer value");
    System.out.println ("How many hands would you like to generate");
         }
    }
    // assigns values to each card and tests to make sure they are not duplicated
    while (counter < numberOfHands) {
        counter++;
      randNum1 = myRandom.nextInt(52) + 1;
      while (cardChecker == false) {
      randNum2 = myRandom.nextInt(52) + 1;
        if (randNum2 != randNum1){
          cardChecker = true;
        }
        randNum2 = myRandom.nextInt(52) + 1;
      }
      cardChecker = false;
      while (cardChecker == false) {
      randNum3 = myRandom.nextInt(52) + 1;
        if (randNum3 != randNum1 && randNum3 != randNum2){
          cardChecker = true;
        }
        randNum3 = myRandom.nextInt(52) + 1;
      }
      cardChecker = false;
      while (cardChecker == false) {
      randNum4 = myRandom.nextInt(52) + 1;
        if (randNum4 != randNum1 && randNum4 != randNum2 && randNum4 != randNum3){
          cardChecker = true;
        }
        randNum4 = myRandom.nextInt(52) + 1;
      }
      cardChecker = false;
      while (cardChecker == false) {
      randNum5 = myRandom.nextInt(52) + 1;
        if (randNum5 != randNum1 && randNum5 != randNum2 && randNum5 != randNum3 && randNum5 != randNum4 ){
          cardChecker = true;
        }
        randNum5 = myRandom.nextInt(52) + 1;
      }
      cardChecker = false;

      // face value for each card
      num1Value = randNum1 % 13;
      num2Value = randNum2 % 13;
      num3Value = randNum3 % 13;
      num4Value = randNum4 % 13;
      num5Value = randNum5 % 13;
      
      // tests to see if there is any pairs at all
      if (num1Value == num2Value || num1Value == num3Value || num1Value == num4Value || num1Value == num5Value) {
        pairs = true;
      }
      if (num2Value == num1Value || num2Value == num3Value || num2Value == num4Value || num2Value == num5Value) {
        pairs = true;
      }
      if (num3Value == num1Value || num3Value == num2Value || num3Value == num4Value || num3Value == num5Value) {
        pairs = true;
      }
      if (num4Value == num1Value || num4Value == num3Value || num4Value == num2Value || num4Value == num5Value) {
        pairs = true;
      }
      if (num5Value == num1Value || num5Value == num3Value || num5Value == num4Value || num5Value == num2Value) {
        pairs = true;
      }

      // runs if there are pairs
      while (pairs) {
              
        // stes value of pairOne and pairTwo based on the values that are the same
              if (num1Value == num2Value) {
                  pairOne = num1Value;
              }
              if (num1Value == num3Value) {
                  pairOne = num1Value;
              }
              if (num1Value == num4Value) {
                  pairOne = num1Value;
              }
              if (num1Value == num5Value) {
                  pairOne = num1Value;
              }
              if (num2Value == num3Value && num2Value != pairOne && pairOne != 0) {
                  pairTwo = num2Value;
              }
              if (num2Value == num3Value && pairOne == 0) {
                  pairOne = num2Value;
              }
              if (num2Value == num4Value && num2Value != pairOne && pairOne != 0) {
                  pairTwo = num2Value;
              }
              if (num2Value == num4Value && pairOne == 0) {
                  pairOne = num2Value;
              }
              if (num2Value == num5Value && num2Value != pairOne && pairOne != 0) {
                  pairTwo = num2Value;
              }
              if (num2Value == num5Value && pairOne == 0) {
                  pairOne = num2Value;
              }
              if (num3Value == num4Value && num3Value != pairOne && pairOne != 0) {
                  pairTwo = num3Value;
              }
              if (num3Value == num4Value && pairOne == 0) {
                  pairOne = num3Value;
              }
              if (num3Value == num5Value && num3Value != pairOne && pairOne != 0) {
                  pairTwo = num3Value;
              }
              if (num3Value == num5Value && pairOne == 0) {
                  pairOne = num3Value;
              }
              if (num4Value == num5Value && num3Value != pairOne && pairOne != 0) {
                  pairTwo = num3Value;
              }
              if (num4Value == num5Value && pairOne == 0) {
                  pairOne = num3Value;
              }
              
        // increments pairChecker and pair2Checker based on how many pairs there are
              if (num1Value == pairOne) {
                  pairChecker++;
              }
              if (num2Value == pairOne) {
                  pairChecker++;
              }
              if (num3Value == pairOne) {
                  pairChecker++;
              }
              if (num4Value == pairOne) {
                  pairChecker++;
              }
              if (num5Value == pairOne) {
                  pairChecker++;
              }
              
              if (num1Value == pairTwo) {
                  pair2Checker++;
              }
              if (num2Value == pairTwo) {
                  pair2Checker++;
              }
              if (num3Value == pairTwo) {
                  pair2Checker++;
              }
              if (num4Value == pairTwo) {
                  pair2Checker++;
              }
              if (num5Value == pairTwo) {
                  pair2Checker++;
              }
        
        
        
        
        
        pairs = false;
      }
      
      // adds to probabilities based on pair checkers
      if (pairChecker == 2 && pair2Checker == 0) {
    	  onePair++;
      }
      if (pairChecker == 3 && pair2Checker == 0) {
    	  threeOfAKind++;
      }
      if (pairChecker == 4 && pair2Checker == 0) {
    	  fourOfAKind++;
      }
      
      if (pairChecker == 2 && pair2Checker == 1) {
    	  onePair++;
      }
      if (pairChecker == 3 && pair2Checker == 1) {
    	  threeOfAKind++;
      }
      if (pairChecker == 4 && pair2Checker == 1) {
    	  fourOfAKind++;
      }
      if (pairChecker == 1 && pair2Checker == 4) {
    	  fourOfAKind++;
      }
      if (pairChecker == 1 && pair2Checker == 3) {
    	  threeOfAKind++;
      }
      
      if (pairChecker == 2 && pair2Checker == 2) {
    	  twoPair++;
      }
      if (pairChecker == 2 && pair2Checker == 3) {
    	  threeOfAKind++;
        onePair++;
      }

      if (pairChecker == 3 && pair2Checker == 2) {
          onePair++;
          threeOfAKind++;
      }
      // resets variables
      pairOne = 0;
      pairTwo = 0;
      pairChecker = 0;
      pair2Checker = 0;
  
    }
    // initializes and prints probabilities
    double onePairProbability = ((double)onePair) / ((double) numberOfHands);
    double twoPairProbability = ((double)twoPair) / ((double) numberOfHands);
    double threeOfAKindProbability = ((double)threeOfAKind) / ((double) numberOfHands);
    double fourOfAKindProbability = ((double)fourOfAKind) / ((double) numberOfHands);
    
    
    System.out.println("Number of loops: " + numberOfHands);
    System.out.printf("Probability of four-of-a-kind: %.3f\n",fourOfAKindProbability);
    System.out.printf("Probability of three-of-a-kind: %.3f\n",threeOfAKindProbability);
    System.out.printf("Probability of two-pair: %.3f\n",twoPairProbability);
    System.out.printf("Probability of one-pair: %.3f\n",onePairProbability);
    
    
  }
}
