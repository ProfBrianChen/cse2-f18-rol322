//// CSE2 Lab02 Cyclometer
//// Ronan Leahy September 6 2018
//// My bicycle cyclometer (meant to measure speed, distance, etc.) records two kinds of data, the time elapsed in seconds, and the number of rotations of the front wheel during that time. For two trips, given time and rotation count, your program should
///print the number of minutes for each trip
///print the number of counts for each trip
///print the distance of each trip in miles
///print the distance for the two trips combined

public class Cyclometer {
 public static void main (String [] args) {
   int secsTrip1 = 480; // amount of seconds for first trip
   int secsTrip2 = 3220; // amount of seconds for second trip
   int countsTrip1 = 1561; // amount of counts for first trip
   int countsTrip2 = 9037; // amount of counts for second trip
   double wheelDiameter = 27.0, // diameter of bicycle wheel
   PI = 3.14159, // numerical value of pi
   feetPerMile = 5280,
   inchesPerFoot = 12, // number of inches in a foot
   secondsPerMinute = 60; // number of seconds in a minute
   double distanceTrip1 , distanceTrip2 , totalDistance; // distance for each trip as well as combined distance
  
   System.out.println ("Trip 1 took " + (secsTrip1/secondsPerMinute) + " minutes and had " + countsTrip1 + " counts");
   System.out.println ("Trip 2 took " + (secsTrip2/secondsPerMinute) + " minutes and had " + countsTrip2 + " counts");
  
   distanceTrip1 = countsTrip1 * wheelDiameter * PI; // distance in inches
  
   distanceTrip1 = distanceTrip1 / inchesPerFoot / feetPerMile; //distance in miles
   distanceTrip2 = countsTrip2 * wheelDiameter * PI / inchesPerFoot / feetPerMile;
   totalDistance = distanceTrip1 + distanceTrip2; // toal distance of the first and second trip
  
   System.out.println ("Trip 1 was " + distanceTrip1 + " miles");
   System.out.println ("Trip 2 was " + distanceTrip2 + " miles");
   System.out.println ("The total distance was " + totalDistance + " miles");
  
  
  
 }
}
