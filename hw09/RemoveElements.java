// CSE2 Ronan Leahy hw09


import java.util.*;
public class RemoveElements {
  public static void main(String [] arg){
	Scanner scan=new Scanner(System.in);
int num[]=new int[10];
int newArray1[];
int newArray2[];
int index,target;
	String answer="";
	do{
  	System.out.print("Random input 10 ints [0-9]");
  	num = randomInput();
  	String out = "The original array is:";
  	out += listArray(num);
  	System.out.println(out);
 
  	System.out.print("Enter the index ");
  	index = scan.nextInt();
        
        // checks for out of bounds
        while (index < 0 || index > 10) {
            System.out.println("ERROR: Out of bounds");
            System.out.print("Enter the index ");
            index = scan.nextInt();
        }
        
  	newArray1 = delete(num,index);
  	String out1="The output array is ";
  	out1+=listArray(newArray1); //return a string of the form "{2, 3, -9}"  
  	System.out.println(out1);
 
      System.out.print("Enter the target value ");
  	target = scan.nextInt();
  	newArray2 = remove(num,target);
  	String out2="The output array is ";
  	out2+=listArray(newArray2); //return a string of the form "{2, 3, -9}"  
  	System.out.println(out2);
  	 
  	System.out.print("Go again? Enter 'y' or 'Y', anything else to quit-");
  	answer=scan.next();
	}while(answer.equals("Y") || answer.equals("y"));
  }
 
    public static String listArray(int num[]){
	    String out="{";
	      for(int j=0;j<num.length;j++){
                if(j>0){
                    out+=", ";
                }
                out+=num[j];
             }
	          out+="} ";
	    return out;
   }
  
  public static int[] randomInput() {
      Random rand = new Random();
      int [] array = new int[10];
      // randomizes an array with ints from 0-9
      for (int i = 0; i < array.length; i++) {
          int x = rand.nextInt(10);
          array[i] = x;
      }
      return array;
  }
  
  public static int [] delete(int [] list,int pos) {
      int [] arrayDeleted = new int [9]; // new array
      boolean deleted = false;
      for (int i = 0; i < list.length; i++) {
          // checks if the array value is equal to the position input
          if (i == pos) {
              deleted = true;
              continue;
          }
          // otherwise adds index to new array
        if (deleted == false) {
          arrayDeleted[i] = list [i];
        }
        if (deleted) {
          arrayDeleted[i-1] = list [i];
        }
      }
      // returns array
      return arrayDeleted;
  }
  
  public static int [] remove (int [] list, int target) {
    int arrayLength = 0;
    for (int i = 0; i < list.length; i++) {
      if(list[i] == target) {
        arrayLength++;
      }
    }
      int [] arrayRemoved = new int [10 - arrayLength]; // new array
      int x = list.length;
      int y = 0;
      for (int i = 0; i < x; i++) {
          // if list value at index i equals the target then it continues loop
          if (list[i] == target) {
            continue;   
          }
          // otherwise sets it equal to new array
        if (list[i] != target) {
          arrayRemoved[y] = list [i];
          y++;

        }
       
      }
      // returns array
      return arrayRemoved;
  }
}


