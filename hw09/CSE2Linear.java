// CSE2 Ronan Leahy hw09
// Using linear and binary searches to search a user-made array

import java.util.*;
public class CSE2Linear {
  public static void main(String[] args) {
        Scanner scan = new Scanner (System.in); // scanner
        int counter = 0; // counts number of numbers entered
        int number = 0; // user inputs
        int searchedNumber = 0; // number they want to search for
        int [] array1 = new int[15]; // array of user inpit
        int pastNumber = 0; // holds value for previous number
            System.out.println("Enter 15 ascending ints for final grades in CSE2:");
            System.out.println("Please input an integer from 0-100");
        while (counter < 15) {
            if (scan.hasNextInt() == false) {
                System.out.println ("ERROR: Input an integer"); // error for non-ints
                scan.nextLine();
                continue;
            }
            if (scan.hasNextInt()) {
                number = scan.nextInt();
                if (number < 0 || number > 100) {
                    System.out.println ("ERROR: Number must be between 0-100"); // error for out of bounds
                    continue;
                }
                if (number < pastNumber) {
                    System.out.println("ERROR: Must be bigger than last number"); // error for not ascending
                    continue;
                }
                if (number >= 0 && number <= 100) {
                    // enters values into array and initializes pastNumber
                    array1 [counter] = number;
                    pastNumber = array1 [counter];
                    counter++;
                }
                
            }
        }
        // prints array
        for (int i = 0; i < array1.length; i++) {
        System.out.print (array1[i] + " ");
        }
        
        // prompts user to enter grade
        System.out.println("Enter a grade to search for:");
        while (searchedNumber == 0) {
        if (scan.hasNextInt()) {
            searchedNumber = scan.nextInt();
        }
        else {
            System.out.println("ERROR");
        }
    }
        // binary search
        binarySearch(searchedNumber, array1);
        
        // array scrambler
        scramble (array1);
        
        System.out.println("Scrambled");
        
        // prints array
        for (int i = 0; i < array1.length; i++) {
        System.out.print (array1[i] + " ");
        }
        
        // second searched number
        System.out.println("Enter a grade to search for:");
        searchedNumber = 0;
        while (searchedNumber == 0) {
        if (scan.hasNextInt()) {
            searchedNumber = scan.nextInt();
        }
        else {
            System.out.println("ERROR");
        }
    }
        // linear seach
        linearSearch(searchedNumber, array1);
    }
    
    public static void binarySearch (int search, int [] array) {
        int counterSearch = 1; // number of iterations
        int low = array[0]; // first element in array
        int high = array[14]; // last element in array
        boolean searched = false; // determines whether number is found
        boolean lower = false; 
        boolean higher = false;
        int length = array.length; // length of array
        int mid = length / 2; // middle of array
        
            while (low<=high) { 

            if (array[mid] > search) {
                high = array[mid]; // sets new max
                mid = mid -1;
                if (mid == -1) {
                    break;
                }
                lower = true;
                if (higher) {
                    break;
                }
            }
            else if (array[mid] < search) {
                low = array[mid]; // sets new min
                mid = mid + 1;
                if (mid == 15) {
                    break;
                }
                higher = true;
                if (lower) {
                    break;
                }
            }
            // breaks loop if mid is equal to search
            else if (array[mid] == search) {
                System.out.println(search + " was found in the list with " + counterSearch + " iterations");
                searched = true;
                break;
            } 
            else {
                break;
            }
            counterSearch++;
            
    }
            if (searched == false){
                System.out.println(search + " was not found in the list with " + counterSearch + " iterations");
            }
            
    }
    
    public static void scramble (int [] array) {
        Random rand = new Random();
        int temp;
        
        // randomizes every position of the array
        for (int i = 0; i < array.length; i++){
           int x = rand.nextInt(15);
           temp = array[i];
           array[i] = array[x];
           array[x] = temp;
        }
        
        
    }
    
    public static void linearSearch (int search, int[] array) {
        int counter = 0;
        boolean found = false;
        
        // goes through each element of the array to check for the searched number
        for (int i = 0; i < array.length; i++) {
            if (array[i] == search) {
                counter++;
                found = true;
                break;
            }
            else {
                counter++;
            }
        }
        if (found) {
            System.out.println (search + " was found in the list with " + counter + " iterations");
        }
        if (!found) {
            System.out.println (search + " was not found in the list with " + counter + " iterations");
        }
    }
    
}


