// Testing code for exam1


import java.util.*;

public class Exam1Test {
  public static void main (String [] args) {
    //System.out.println (1 + 1 + "day" + 2 + 3); returns value 2day23
    //System.out.println (3 * 4 == 7 > 5); incomparable types int and boolean
    //System.out.println (2 < 3 || 5 * 2 == 11 && 5 > 6); returns true because lazy or operator
    //System.out.println (3 % 12 + 5 * 4 % 11 / 2.0); returns value 7.5
    //System.out.println (2 < 0 && 3 / (1-1) > 0); returns false because lazy and operator
    
   /* The code below states n is not initialized because only m is set to the value 6 and
      n is only given a value inside the if statement scope
    int n, m = 6;
      if (m>5) {
        n = 4;
      }
    
    System.out.println ("n=" + n);
    */
    
    /*The code below returns the value 28 because n * m is 35, * 2 is 70 and % 6 returns a remainder
    of 4 which then runs the if statement to make k = 20. The second if statement runs as well which
    then adds 8 more to k making it 28
    int n = 5, m = 7, k = 0;
      if (n * m * 2 % 6 == 4) {
        k = 20;
      }
    if ((n + m) / .5 > 10) {
      k += 8;
    }
    System.out.println ("k=" + k);
    */
   
    /* Circle 5 logical errors and explain what the error is
      public class One         //(1)
      public static void main(String arg[]){     //(2)
        float x,u;          //(3)
	   int y;z;            //(4)
        y=2.1;              //(5)
        x=1.2f;   
	   if(z==3){           //(6)
           int v=3;
           z++; 
        }
        System.out.println("The value of x: "+
           x + " y: " + y +" and z: " + v);      //(7) 
                                                 //(8)
                                                 //(9) 
   }
   Error on line 1: No curly bracket to contain the class
   Error on line 3: u is never used
   Error on line 4: there should be a comma, not a semicolon, between y and z
   Error on line 5: y is an integer and cannot be cast as a double
   Error on line 6: z is never initialized
   Error on line 7: Prints a value for v instead of z, v is never declared or initialized
   Error on line 9: Should be another curly bracket to end the main meth
    */
    
    
    
    /* Prints out 11 and 51
    int k = 50;
    int j = 12;
    if(k/j > 2 && k%j > 1){
      k++;
    if( j%k > 14){
      k++;
	    j++;
   }
    else{j--;}
 }
    else{k/=2;} 
    System.out.println(j+ " "+k);
    
  */
   
    /* Compiler error on line 82 because of loss of precision
       Can't implicitly cast an int as a double
  double z = 3.0, y = 2.4;
  int x = 2;

  z = x + y;
  x = z;
  z -= x + y;
  x+= z;
  x += y;
  */

   /* Prints Good Afternoon! Good night! 
    int i = 3;
    int k =2; //[line1]
      switch(i){
	      case 1:
	      case 2:
        case 3:
		          //  [line2]
		  switch(k){
			  case 1:
				System.out.println("Good Morning!");
				break;
			  case 2:				
				System.out.println("Good Afternoon!");
				break;
		}
	      case 4:
		switch(k){
			case 1:
				System.out.println("Good Day!");
				break;
			case 2:				
				System.out.println("Good Night!");
		}
		  break;
	    case 5:
		System.out.println("testing!");
		  break;
	    default: 
		System.out.println("default!");
		break;
}
*/
  
    /*
    Write code that prompts and then accepts three integers from the user, 
    computes the average of the integers, and prints the decimal average 
      via STDOUT. Then, if the average is positive, output the average is
        positive. If the average is zero, write the average is zero.  
        If the average is negative, write the average is negative.  
        This program is written in a file called Average.java 
    Scanner myScanner = new Scanner (System.in);
    int a = myScanner.nextInt();
    int b = myScanner.nextInt();
    int c = myScanner.nextInt();
    int average = (a + b + c) / 3;
    
    if (average > 0) {
      System.out.println ("The average is positive");
    }
    else if (average == 0) {
      System.out.println ("The average is 0");
    }
    else if (average < 0) {
      System.out.println ("The average is negative");
    }
    System.out.println (average);
    */
    
    /*
   String a = "fly";
   String b = "walk";
    if (a.equals(a)) 
      if (b.equals("b")) 
        System.out.println ("one!");
      
      else 
        System.out.println ("not one!");
      
    else 
      System.out.println("not two?");
      */
  /*  Below, write Symmetrit.java, a java program that prompts 
      the user to enter a positive 5-digit int. If such an int 
      is not entered, the program should tell the user the input 
      is bad and then quit. Otherwise, the program should write 
      out whether the number is symmetric, i.e., whether the first 
      and fifth digits are the same and whether the second and fourth
      digits are the same. For example, 12321 is symmetric, while 23412 
      is not */
    
    /*
    Scanner myScanner = new Scanner (System.in);
    System.out.println ("Please input a 5-digit number");
    int input = myScanner.nextInt();
    int val1 = 0;
    int val2 = 0;
    int val3 = 0;
    int val4 = 0;
    if (input >= 10000 && input <= 99999) {
      val1 = input / 10000;
      val2 = (input / 1000) % 10;
      val3 = (input / 10) % 10;
      val4 = (input % 10);
      if (val1 == val4 && val2 == val3) {
        System.out.println ("Your number is symmetrical");
      }
      else {
        System.out.println ("Your number is not symmetrical");
      }
    }
    else {
      System.out.println ("Incorrect input");
      System.exit(0);
    }
    */
    
    Double myDecimal = (double) (3 + 4 / 6);
    System.out.println (myDecimal);

    
  }
}